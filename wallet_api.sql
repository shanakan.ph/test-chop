-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.5.1-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for node_wallet
CREATE DATABASE IF NOT EXISTS `node_wallet` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `node_wallet`;

-- Dumping structure for table node_wallet.crypto
CREATE TABLE IF NOT EXISTS `crypto` (
  `user_id` int(11) DEFAULT NULL,
  `crypto_ex` varchar(50) DEFAULT NULL,
  `amount` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='connect crypto data';

-- Dumping data for table node_wallet.crypto: ~5 rows (approximately)
/*!40000 ALTER TABLE `crypto` DISABLE KEYS */;
INSERT INTO `crypto` (`user_id`, `crypto_ex`, `amount`) VALUES
	(29, 'ETH', 1000),
	(29, 'TH', 93000),
	(29, 'USDT', 2010),
	(29, 'ETC', 500),
	(30, 'ETC', 200);
/*!40000 ALTER TABLE `crypto` ENABLE KEYS */;

-- Dumping structure for table node_wallet.crypto_rate
CREATE TABLE IF NOT EXISTS `crypto_rate` (
  `crypto_from` varchar(30) NOT NULL,
  `crypto_to` varchar(30) NOT NULL,
  `rate` float NOT NULL,
  KEY `crypto_form_crypto_to` (`crypto_from`,`crypto_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='rate of cryptocurrency';

-- Dumping data for table node_wallet.crypto_rate: ~2 rows (approximately)
/*!40000 ALTER TABLE `crypto_rate` DISABLE KEYS */;
INSERT INTO `crypto_rate` (`crypto_from`, `crypto_to`, `rate`) VALUES
	('ETC', 'TH', 0.5),
	('TH', 'ETC', 0.05);
/*!40000 ALTER TABLE `crypto_rate` ENABLE KEYS */;

-- Dumping structure for table node_wallet.crypto_store
CREATE TABLE IF NOT EXISTS `crypto_store` (
  `crypto_ex` varchar(30) NOT NULL,
  `crypto_name` varchar(30) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`crypto_ex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='keep all crypto data';

-- Dumping data for table node_wallet.crypto_store: ~4 rows (approximately)
/*!40000 ALTER TABLE `crypto_store` DISABLE KEYS */;
INSERT INTO `crypto_store` (`crypto_ex`, `crypto_name`, `status`) VALUES
	('ETC', 'Entercar', 'A'),
	('ETH', 'Ethereum', 'A'),
	('TH', 'Thailand', 'A'),
	('USDT', 'Tether', 'A');
/*!40000 ALTER TABLE `crypto_store` ENABLE KEYS */;

-- Dumping structure for table node_wallet.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `tell` varchar(15) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='data user / data admin';

-- Dumping data for table node_wallet.user: ~9 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `username`, `password`, `fname`, `lname`, `email`, `address`, `tell`, `role`) VALUES
	(1, 'obesity', '068816531', 'shanakan', 'ps', 'shanakan@hotmail.com', '11/23 st.firt', '0619652461', 'admin'),
	(2, 'test', '1234', 'test1', 't', 't@homail.com', '11/22 st.ff', '0588683223', 'user'),
	(24, 'qowbes733', '1234', 'oil', 'test', 'qtwes4s3@hotmail.com', 'test', '12345', 'admin'),
	(25, 'qowbses7s33', '1234', 'oil', 'test', 'qtwe3@hotmail.com', 'test', '12345', 'admin'),
	(26, 'qowbes7s33', '1234', 'oil', 'test', 'qtw3@hotmail.com', 'test', '12345', 'admin'),
	(27, 'qewbes7s33', '1234', 'oil', 'test', 'eotmail.com', 'test', '12345', 'admin'),
	(28, 'qewbd7s33', '1234', 'oil', 'test', 'eotdail.com', 'test', '12345', 'admin'),
	(29, 'oil', '1234', 'oil', 'test', 'oil.com', 'test', '12345', 'user'),
	(30, 'oil1', '1234', 'oil', 'test', 'oil1.com', 'test', '12345', 'user');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
