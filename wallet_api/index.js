var app = require('express')();
var jwt = require('jsonwebtoken')
var bodyParser = require('body-parser');
// parse application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

const mariadb = require('mariadb');
const connection = mariadb.createPool({
  host: 'localhost', 
  user:'root', 
  password: 'root',
  connectionLimit: 3,
  database: "node_wallet"
});


var port = process.env.PORT || 7777;
var path = '/api/v1/'


app.post(path+'registers', function (req, res) {
  var json = req.body;
  connection.getConnection()
  .then(conn => {
    conn.query("SELECT*FROM user")
      .then((rows) => {
        for (let index = 0; index < rows.length; index++) {
          const element = rows[index];
          if (json.username == element['username'] || json.email == element['email']) {
            conn.end();
            res.status(404).json({"err" : "Have username or email already" });
            return;         
          }  
        }
        return conn.query("INSERT INTO user(username,password,fname,lname,email,address,tell,role) value (?, ?, ?, ?, ?, ?, ?, ?)",[json.username, json.password, json.fname, json.lname, json.email, json.address, json.tell, json.role]);
      })
      .then((res) => {
        // console.log("Employee Id:- " + res.insertId);
        return conn.query("INSERT INTO crypto(user_id) value (?)",[res.insertId]);
      })
        .then((res) => {
          conn.end();
      })
      .catch(err => {
        console.log(err); 
        conn.end();
        res.json(err)
      })
    }).catch(err => {
      //not connected
    });
    return res.status(202).json({"success" : "Register success." })
});


app.post(path+'login', function (req, res) {
  var json = req.body;
  connection.getConnection()
  // console.log(json);
  .then(conn => {
    conn.query("SELECT*FROM user")
      .then((rows) => {
        console.log(rows); //[ {val: 1}, meta: ... ]
        for (let index = 0; index < rows.length; index++) {
          const element = rows[index];
          if (json.username == element['username'] || json.email == element['password']) {
              const token = jwt.sign({element} , 'my_secret_key');
              conn.end();
              conn.end();
              if(res.statusCode == 200) {
                res.status(200).json({ "token" : token });
                return;
              }         
            }  
        }
        var status = false
        return status
        })
        .then((status) => {
          res.status(404).json({"err" : "Invalid yout username or password" });
          return;
      })
      })
      .catch(err => {
        console.log(err); 
        conn.end();
        res.json(err)
  }).catch(err => {
    //not connected
  });
});


app.post(path+'admin/store',ensureToken, function (req, res) {
  jwt.verify(req.token,'my_secret_key',function(err,data){
    console.log(req.body,"req.body");
    
  const json = req.body
  console.log(json,"json ja");
  
  console.log(json,"<====json");
  if (err) {
      console.log(err);
    
      res.sendStatus(403);
  }else{
    if (data['element']['role'] == 'admin'){
      connection.getConnection()
      .then(conn => {
        conn.query("SELECT*FROM crypto_store")
          .then((rows) => {
            if (rows != {}){
            // console.log(rows); //[ {val: 1}, meta: ... ]
            for (let index = 0; index < rows.length; index++) {
              const element = rows[index];
              console.log(element['crypto_ex'],"1");
              console.log(json.crypto_ex,"2");
              if (json.crypto_ex == element['crypto_ex']) {
                conn.end();
                res.status(400).json({ "err" : "Have crypto name already." });
                return;          
              }  
            }
          return conn.query("INSERT INTO node_wallet.crypto_store(crypto_ex,crypto_name,status) VALUE (?,?,?)",[json.crypto_ex,json.crypto_name,json.status]);
        }
      })
        .then((result) => {
          conn.end();
          res.status(200).json({ "success" : "Add crypto name success." });
          return;  
      })

      }).catch(err => {
          //not connected
        });
    }
  }
  })
});


app.post(path+'admin/rate',ensureToken, function (req, res) {
  jwt.verify(req.token,'my_secret_key',function(err,data){
  const json = req.body
  console.log(json,"<====json");
  if (err) {
    console.log(err);
    res.sendStatus(403);
  }else{
    if (data['element']['role'] == 'admin'){
      connection.getConnection()
      .then(conn => {
        conn.query("SELECT count(*) FROM crypto_store WHERE crypto_ex = (?) or crypto_ex = (?)",[json.crypto_from,json.crypto_to])
          .then((rows) => {
            if (rows != {}){
              console.log(rows[0]["count(*)"]);
              if (rows[0]["count(*)"] == 2){
                conn.query("SELECT count(*) FROM crypto_rate WHERE crypto_from = (?) and crypto_to = (?)",[json.crypto_from,json.crypto_to])
                .then((count) => {
                  console.log(count[0]["count(*)"],"count");
                  if (count[0]["count(*)"] == 0){
                    return conn.query("INSERT INTO node_wallet.crypto_rate(crypto_from,crypto_to,rate) VALUE (?,?,?)",[json.crypto_from,json.crypto_to,json.rate]);
                  }else{
                    R = { "success" : "Have exchange rate "+json.crypto_from+' to '+ json.crypto_to+' already'}
                    console.log(R);
                    conn.end();
                    res.status(200).json(R);
                    return;  
                  }

                })
                .then((success) => {
                  console.log();
                  conn.end();
                    res.status(200).json('Add crypto exchange rate succes');
                    return; 
                  })
                .catch(err => {
                  //not connected
                });
              }else{
                conn.end();
                res.status(400).json({ "err" : "No Data of your crypto name." });
                return;  
              }
              
          }
        })
      }).catch(err => {
          //not connected
      });
    }
  }
  })
});


app.post(path+'topup',ensureToken, function (req, res) {
  jwt.verify(req.token,'my_secret_key',function(err,data){
    const json = req.body
    console.log(json,"<====json");
    if (err) {
        console.log(err);
        res.sendStatus(403);
    } else{
      if (data['element']['role'] == 'user'){
        connection.getConnection()
        .then(conn => {
          conn.query("SELECT count(*) FROM crypto_store WHERE crypto_ex = (?)",[json.crypto])
            .then((rows) => {
              if (rows[0]["count(*)"] == 1){
                conn.query("SELECT count(*),amount FROM node_wallet.crypto WHERE user_id = (?) and crypto_ex = (?)",[data['element']['user_id'],json.crypto])
                  .then((count) => {
                    if (count[0]["count(*)"] == 1){
                      var money = parseFloat(count[0]["amount"]);
                      var new_money = parseFloat(json.amount);
                      money = money+new_money
                      conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[money,data['element']['user_id'],json.crypto])
                      .then((lresult) => {
                        res.status(200).json({ "success" : "Top up your money success." });
                        var status = true
                        return status
                    }) 
                  }else{
                    conn.query("INSERT INTO node_wallet.crypto(user_id,crypto_ex,amount) VALUE (?,?,?)",[data['element']['user_id'],json.crypto,json.amount]);
                    conn.end();
                    res.status(200).json({ "success" : "Top up your money success." });
                    return;
                  }
              })
         }else{
                conn.end();
                res.status(400).json({ "err" : "Plases check your currency." });
                return;  
              }
          })
        }).catch(err => {
            //not connected
          });
        }
      }
  })
});


app.post(path+'buy',ensureToken, function (req, res) {
  jwt.verify(req.token,'my_secret_key',function(err,data){
    const json = req.body
    console.log(json,"<====json");
    if (err) {
        console.log(err);
        res.sendStatus(403);
    } else{
      console.log(data['element']);
      if (data['element']['role'] == 'user'){
        connection.getConnection()
        .then(conn => {
          conn.query("SELECT count(*),amount FROM crypto WHERE user_id = (?) and crypto_ex = (?)",[data['element']['user_id'],json.crypto_from])
            .then((rows) => {
              if (rows[0]["count(*)"] == 1){
                conn.query("SELECT count(*),rate FROM crypto_rate WHERE crypto_from = (?) and crypto_to = (?)",[json.crypto_from,json.crypto_to])
                .then((rows_rate) => {
                  if (rows_rate[0]["count(*)"] == 1){
                    var money = parseFloat(rows[0]["amount"]);
                    var new_money = parseFloat(json.money);         
                    if (new_money > money){
                      res.status(200).json({ "success" : "Your money not enough to exchenge." });
                      return;
                    }else{
                      conn.query("SELECT amount FROM crypto WHERE user_id = (?) and crypto_ex = (?)",[data['element']['user_id'],json.crypto_to])
                      .then((chenge) => {
                        var exchenge = money*new_money*rows_rate[0]["rate"] 
                        money = money-new_money
                        new_money = exchenge+chenge[0]["amount"]
                           // update
                        conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[money,data['element']['user_id'],json.crypto_from])
                        .then((update_ex) => {
                          console.log(update_ex,'update_ex');
                          conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[new_money,data['element']['user_id'],json.crypto_to])
                          .then((update_exx) => {
                            console.log(update_exx,'update_exx');
                            res.status(200).json({ "success" : "Success exchenge for your currency." });
                            return;
                          })
                        })
                      })   
                    }
                  }else{
                    res.status(200).json({ "success" : "Your id don't have this currency." });
                    return;
                  }
                })
              }else{
                res.status(200).json({ "success" : "No exchenge for your currency now." });
                return;
              }
          })
        }).catch(err => {
          console.log(err);
            //not connected
          });  
        }
      }
  })
});



app.post(path+'exchange',ensureToken, function (req, res) {
  jwt.verify(req.token,'my_secret_key',function(err,data){
    const json = req.body
    console.log(json,"<====json");
    if (err) {
        console.log(err);
        res.sendStatus(403);
    } else{
      console.log(data['element']);
      if (data['element']['role'] == 'user'){
        connection.getConnection()
         // select money from crypto that money enongth to send os exchenge to each other => ok
              // then => check user that you to exchang e that are real => ok
              // check if cryto from ex TH = TH can send to that user sentary
              // exchenge with from + cryptp from to to + crypto to
        .then(conn => {
          conn.query("SELECT count(*) FROM user WHERE user_id = (?)",[json.exchengeid])
          .then((user) => {
            // console.log(user,'user');
            console.log(user[0]["count(*)"]);
            console.log(data['element']['user_id'],"------",json.crypto_from);
            
            if (user[0]["count(*)"] == 1){
              conn.query("SELECT count(*),amount FROM crypto WHERE user_id = (?) and crypto_ex = (?)",[data['element']['user_id'],json.crypto_from])
              .then((rows) => {
                console.log(rows);
                
                if (rows[0]["count(*)"] == 1){
                  var money = parseFloat(rows[0]["amount"]);
                  var new_money = parseFloat(json.money);      
                  var old_owner = money-json.money   
                  if (new_money > money){
                    res.status(200).json({ "success" : "Your money not enough to exchenge." });
                    return;
                  }else{
                    // update money of 2 user
                    
                    if (json.crypto_from == json.crypto_to){
                      var new_owner = user[0]["amount"] + json.money
                      //  check cryoto in db that ownwe have crypto name
                      conn.query("SELECT count(*),amount FROM node_wallet.crypto WHERE user_id = (?) and crypto_ex = (?)",[json.exchengeid,json.crypto_to])
                      .then((check) => {
                        if (check[0]["count(*)"] == 1){
                          console.log(old_owner,"----",data['element']['user_id'],"-----",json.crypto_form);
                          
                          conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[old_owner,data['element']['user_id'],json.crypto_form])
                          .then((update) => {
                            console.log(update);
                            conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[new_owner,json.exchengeid,json.crypto_to])
                            .then((success) => {
                              console.log(success);
                              res.status(200).json({ "success" : "Success to exchenge." });
                              return;
                            })
                          })
                        }else{
                          conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[old_owner,data['element']['user_id'],json.crypto_form])
                          .then((update) => {
                            console.log(update);
                            conn.query("INSERT INTO node_wallet.crypto(crypto_ex,amount) VALUE (?,?)  WHERE user_id = (?)",[json.crypto_to,new_owner,json.exchengeid])
                            .then((success) => {
                              console.log(success);
                              res.status(200).json({ "success" : "Success to exchenge." });
                              return;
                            })
                          })

                        }
                      })
                    }else{
                      // console.log(check[0]["count(*)"],'<=exchange');
                      
                      // sql rate of from to then use from*rate = new money
                      console.log("go to exchenge sql with rate");
                      conn.query("SELECT count(*),rate FROM crypto_rate WHERE crypto_from = (?) and crypto_to = (?)",[json.crypto_from,json.crypto_to])
                      .then((success) => {
                        console.log(success);
                        console.log(success);
                        console.log(success[0]["count(*)"],"<= check");
                        
                        if (success[0]["count(*)"] == 1 ){ //mean have rate with this currency
                          var rate = parseFloat(success[0]["rate"]);
                          // var new_owner = json.money*rate
                          
                          // var rate = parseFloat(success[0]["rate"]);
                          var money = parseFloat(json.money);
                          
                      //  check cryoto in db that owner have crypto name
                          console.log(json.exchengeid,'------',json.crypto_to);
                      
                          conn.query("SELECT count(*),amount FROM node_wallet.crypto WHERE user_id = (?) and crypto_ex = (?)",[json.exchengeid,json.crypto_to])
                          .then((check) => {
                            if (check[0]["count(*)"] == 1){
                              var owner_money = parseFloat(check[0]["amount"]);
                              var new_owner = json.money*rate+owner_money
                              console.log(old_owner,"-]]-",data['element']['user_id'],"--]]--",json.crypto_from);
                              
                              conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[old_owner,data['element']['user_id'],json.crypto_from])
                              .then((update) => {
                                console.log(update);
                                conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[new_owner,json.exchengeid,json.crypto_to])
                                .then((success) => {
                                  console.log(success);
                                  res.status(200).json({ "success" : "Success to exchenge." });
                                  return;
                                })
                              })
                            }else{
                              console.log("count = 0 in exchenge");
                              console.log(old_owner,"===",data['element']['user_id'],"===",json.crypto_from);
                              
                              conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[old_owner,data['element']['user_id'],json.crypto_from])
                              .then((update) => {
                                console.log("3333-3-");
                                var rate = parseFloat(success[0]["rate"]);
                                var money = parseFloat(json.money);
                                var new_owner = money*rate
                                conn.query("INSERT INTO node_wallet.crypto(user_id,crypto_ex,amount) VALUE (?,?,?)",[json.exchengeid,json.crypto_to,new_owner])
                                .then((update) => {
                                  console.log(success);
                                  res.status(200).json({ "success" : "Success to exchenge. 1" });
                                  return;
                                })
                                // test find money of owner
                                // conn.query("SELECT count(*),amount FROM crypto WHERE user_id = (?) and crypto_ex = (?)",[json.exchengeid,json.crypto_to])
                                // .then((check) => {
                                  // console.log();
                                  // console.log(check);
                                  // console.log(json.exchengeid,"===",json.crypto_to,"===",new_owner,"===",check[0]["amount"]);
                                  // if (check[0]["count(*)"] == 1){
                                    // console.log(check);
                                    // console.log(json.exchengeid,"===",json.crypto_to,"===",new_owner,"===",check[0]["amount"]);
                                    
                                  // }
                                // })
                                // console.log(update);
                                // conn.query("INSERT INTO node_wallet.crypto(user_id,crypto_ex,amount) VALUE (?,?,?)",[json.exchengeid,json.crypto_to,new_owner])
                                // .then((success) => {
                                  
                                //   console.log(success);
                                //   res.status(200).json({ "success" : "Success to exchenge." });
                                //   return;
                                // })
                              })

                            }
                          })
                          // conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[old_owner,data['element']['user_id'],json.crypto_form])
                          // .then((update) => {
                          //   console.log(update);
                          //   conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[new_owner,json.exchengeid,json.crypto_to])
                          //   .then((success) => {
                          //     console.log(success);
                          //     res.status(200).json({ "success" : "Success to exchenge." });
                          //     return;
                          //   })
                          // })
                      
                        }else{
                        res.status(400).json({ "err" : "Not have your currency rate in my system." });
                        return;
                        }
                      })
                    }
                  }
                }
              })
            }else{
              res.status(400).json({ "success" : "Assignee id is False." });
              return;
            }
          })
          // conn.query("SELECT count(*),amount FROM user WHERE user_id = (?)",[data['element']['user_id']])
          //   .then((rows) => {
             
              // if (rows[0]["count(*)"] == 1){
              //   var money = parseFloat(rows[0]["amount"]);
              //   var new_money = parseFloat(json.money);         
              //   if (new_money > money){
              //     res.status(200).json({ "success" : "Your money not enough to exchenge." });
              //     return;
              //   }else{
              //     if (json.crypto_from == json.crypto_to){

              //     }
                  // conn.query("SELECT count(*) FROM crypto WHERE user_id = (?)",[json.crypto_from])
                // }
                // conn.query("SELECT count(*),amount FROM crypto WHERE user_id = (?)",[json.crypto_from])
              



              //   conn.query("SELECT count(*),rate FROM crypto_rate WHERE crypto_from = (?) and crypto_to = (?)",[json.crypto_from,json.crypto_to])
              //   .then((rows_rate) => {
              //     if (rows_rate[0]["count(*)"] == 1){
              //       var money = parseFloat(rows[0]["amount"]);
              //       var new_money = parseFloat(json.money);         
              //       if (new_money > money){
              //         res.status(200).json({ "success" : "Your money not enough to exchenge." });
              //         return;
              //       }else{
              //         conn.query("SELECT amount FROM crypto WHERE user_id = (?) and crypto_ex = (?)",[data['element']['user_id'],json.crypto_to])
              //         .then((chenge) => {
              //           var exchenge = money*new_money*rows_rate[0]["rate"] 
              //           money = money-new_money
              //           new_money = exchenge+chenge[0]["amount"]
              //              // update
              //           conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[money,data['element']['user_id'],json.crypto_from])
              //           .then((update_ex) => {
              //             console.log(update_ex,'update_ex');
              //             conn.query("UPDATE node_wallet.crypto SET amount = (?) WHERE user_id = (?) and crypto_ex = (?)",[new_money,data['element']['user_id'],json.crypto_to])
              //             .then((update_exx) => {
              //               console.log(update_exx,'update_exx');
              //               res.status(200).json({ "success" : "Success exchenge for your currency." });
              //               return;
              //             })
              //           })
              //         })   
              //       }
              //     }else{
              //       res.status(200).json({ "success" : "Your id don't have this currency." });
              //       return;
              //     }
              //   })
              // }else{
              //   res.status(200).json({ "success" : "Assignee id is False." });
              //   return;
              // }
          // })
        }).catch(err => {
          console.log(err);
        //     //not connected
          });  
        }
      }
  })
});


function ensureToken(req ,res ,next){
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== 'undefined'){
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer['1'];
      req.token = bearerToken;
      next();
  }else{
      res.sendStatus(403);
  }
}

app.listen(port, function() {
	console.log('Starting node.js on port ' + port);
});